// import { describe, it, expect, beforeEach, type Mock, vi } from 'vitest'

// import { mount, shallowMount } from '@vue/test-utils'
// import LoginForm from '@/components/auth/LoginForm.vue'
// import { createPinia, setActivePinia } from 'pinia'
// import router from '@/router'
// import { useRoute, useRouter, type RouteLocation, type NavigationGuardWithThis } from 'vue-router'

// const mountOptions = {
//     global: {
//         plugins: [router] // router here
//     }
// }

// vi.mock('vue-router');

// const handlePush = (r: RouteLocation) => {
//     const route = useRoute()
//     route.path = r.path;
//     route.fullPath = r.fullPath;
//     route.name = r.name;
// }

// const handleBeforeEach = () => {
//     // const route = useRoute()
//     // guard(route, undefined, () => {})
// }

// const mockRouter = (useRouter as Mock).mockReturnValue({
//     push: vi.fn(handlePush),
//     beforeEach: vi.fn(handleBeforeEach)
// })

// // exec avant test (it)
// beforeEach(() => {
//     setActivePinia(createPinia())
//     mockRouter.mockClear();
//     useRoute().path = '/login'
//     useRoute().fullPath = '/login'
//     useRoute().name = 'login'
// })

// describe('LoginForm', () => {

//     it('renders properly', () => {
//         const wrapper = mount(LoginForm, {
//             global: {
                
//             }
//         })

//         expect(wrapper.text()).toContain('Login')
//     })
    
//     it('router push after login', () => {
//         const routerx = useRouter()
//         const spyPush = vi.spyOn(routerx, 'push')
        
//         const wrapper = mount(LoginForm, {
//             global: {
//                 mocks: {
//                     $router: mockRouter,
//                 },
//                 plugins: [router]
//             }
//         })
        
//         const emailInput = wrapper.find('#email')
//         const passwordInput = wrapper.find('#password')
//         emailInput.setValue('test@test.com')
//         passwordInput.setValue('test')
        
//         const button = wrapper.find('button')
//         button.element.click()

//         expect(wrapper.vm.$route.name).toBe('map')
//     })

// })
