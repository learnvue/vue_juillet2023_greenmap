import type { Feature } from "geojson"
import type { Polygon } from "leaflet"

export type Position = [number, number] // Tuple

export interface Area {
    polygon: Position[]
}

// export interface Feature {
//     properties: {
//         [key: string]: string
//     }
//     geometry: {
//         coordinates: Area,
//         type: string
//     },
//     type: string
// }

export interface FeatureWithId extends Feature {
    id: number
}

export interface FeatureProperties {
    [key: string]: string
}

/*
var polygon = L.polygon([
    [51.509, -0.08],
    [51.503, -0.06],
    [51.51, -0.047]
]).addTo(map);
*/
