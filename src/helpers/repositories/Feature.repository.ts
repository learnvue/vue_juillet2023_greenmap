import type { FeatureWithId } from '@/types/map.types'
import { Axios } from 'axios'
import type { Feature } from 'geojson'

export class FeatureRepository {
    constructor(
        private axios: Axios
    ) {}
    
    add(feature: Feature) {
        return this.axios.post<FeatureWithId>('/features', feature)
    }
    
    getAll() {
        return this.axios.get<FeatureWithId[]>('/features')
    }
}
