

interface IResponse<T> {
    data: T
}

function post<T>(someData: any): IResponse<T> {
    
    const response: IResponse<T> = {
        data: {}
    }
    
    return response
}
