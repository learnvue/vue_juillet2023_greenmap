import { createRouter, createWebHistory } from 'vue-router'
import MapView from '../views/MapView.vue'
import { useAuthStore } from '@/stores/auth'
import { AuthAccess } from '@/types/auth.types'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue'),
      meta: {
        auth: AuthAccess.NON_AUTH
      }
    },
    {
      path: '/map',
      name: 'map',
      component: MapView,
      meta: {
        auth: AuthAccess.AUTH
      }
    },
  ]
})

// to = la page vers laquelle on va
// from = la d'ou on vient
// next, c'est un callback pour passer a la prochaine page (to)
router.beforeEach((to, from, next) => {
  const authStore = useAuthStore()
  
  if (authStore.isAuth && to.meta.auth === AuthAccess.NON_AUTH) {
    return next('/map')
  }
  if (!authStore.isAuth && to.meta.auth === AuthAccess.AUTH) {
    return next('/login')
  }
  
  next()
})

export default router
