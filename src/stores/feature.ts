import { defineStore } from "pinia";
import { Map, Polygon } from 'leaflet'
import type { Feature } from 'geojson'
import type { FeatureProperties } from "@/types/map.types";
import { featureRepository } from "@/helpers/api";

interface FeatureStoreState {
    features: Feature[],
    isEditing: boolean,
    editing: Feature | undefined,
}

export const useFeatureStore = defineStore('feature', {
    state: (): FeatureStoreState => ({
        features: [],
        isEditing: false,
        editing: undefined
    }),
    getters: {
        getEditingProperties(state) {
            if (!state.editing || !state.editing.properties) return []
            return Object.keys(state.editing.properties).map(k => {
                return { key: k, value: state.editing!.properties![k] }
            })
        },
    },
    actions: {
        add(polygon: Polygon) {
            const feature = polygon.toGeoJSON() as Feature
            if (!feature.properties) {
                feature.properties = {}
            }
            this.editing = feature
            this.isEditing = true            
        },
        addProperty(name: string, value: string) {
            if (!this.editing || !this.editing.properties) return ;
            this.editing.properties[name] = value
        },
        async addAndFetch(feature: Feature) {
            await featureRepository.add(feature)
            const { data } = await featureRepository.getAll()
            this.features = data
            this.isEditing = false;
            this.editing = undefined;
        }
    }
})
