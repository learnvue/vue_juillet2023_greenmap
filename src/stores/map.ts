import { defineStore } from "pinia";
import { Map } from 'leaflet'

interface MapStoreState {
    map: Map | undefined,
}

export const useMapStore = defineStore('map', {
    state: (): MapStoreState => ({
        map: undefined
    }),
    actions: {
        startDraw() {
            if (!this.map) return ;
            this.map.pm.enableDraw('Polygon', {
                snappable: true,
                snapDistance: 20,
            });
        }
    }
})
