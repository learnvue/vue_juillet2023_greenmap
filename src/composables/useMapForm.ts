import { storeToRefs } from "pinia"
import { useFeatureStore } from "@/stores/feature"
import type { FeatureProperties } from "@/types/map.types"
import { computed, watch, ref } from "vue"

export const useMapForm = () => {
    const featureStore = useFeatureStore()
    const { editing, getEditingProperties, features, isEditing } = storeToRefs(featureStore)

    const properties = ref(getEditingProperties.value)

    watch(() => featureStore.editing, () => {
        properties.value = getEditingProperties.value
    })

    const finalProps = computed(
        () => {
            const props: FeatureProperties = {}
            properties.value.forEach(p => props[p.key] = p.value)
            return props
        }
    )

    const json = computed(() => JSON.stringify(finalProps.value, null, 4))

    const addProp = () => {
        properties.value.push({
            key: '',
            value: ''
        })
    }
    
    const setEditingProps = () => {
        editing.value!.properties = finalProps.value
    }
    
    return {
        editing,
        isEditing,
        features,
        json,
        addProp,
        properties,
        setEditingProps,
        addAndFetch: featureStore.addAndFetch
    }

}
