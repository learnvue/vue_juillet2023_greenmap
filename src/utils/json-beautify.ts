export function beautifyJson(obj: any): string {
    return JSON.stringify(obj, null, 4)
}
